import { IsEmail, IsEmpty, IsNotEmpty } from 'class-validator';

export class ResetPasswordRequestDto {
  @IsNotEmpty()  
  @IsEmail()
  email: string;
}
