import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { RegisterDto } from './dto/register.dto';
import * as bcryptjs from 'bcryptjs';
import { LoginDto } from './dto/login.dto';
import { JwtService } from '@nestjs/jwt';
import { ResetPasswordRequestDto } from './dto/reset-request-password.dto';
import { v4 as uuidv4 } from 'uuid';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { MoreThanOrEqual } from 'typeorm';

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
        private readonly mailerService: MailerService,
    ) {}

    async register(registerDto: RegisterDto) {
        const existingUser = await this.usersService.findOneByEmail(registerDto.email);
        if (existingUser) {
            throw new BadRequestException('User already exists');
        }
        const hashedPassword = await bcryptjs.hash(registerDto.password, 10);
        const user = {
            ...registerDto, // Copia todos los campos de registerDto
            password: hashedPassword // Asigna la contraseña hasheada
        };
        await this.usersService.create(user);
    }
    

    async login(loginDto: LoginDto) {
        const user = await this.usersService.findOneByEmail(loginDto.email);
        if (!user) {
            throw new UnauthorizedException('Email is wrong');
        }
        const isPasswordValid = await bcryptjs.compare(loginDto.password, user.password);
        if (!isPasswordValid) {
            throw new UnauthorizedException('Password is wrong');
        }
        const payload = { email: user.email };
        const token = await this.jwtService.signAsync(payload);
        return { token, email: user.email };
    }

    async requestPasswordReset(resetPasswordRequestDto: ResetPasswordRequestDto) {
        const { email } = resetPasswordRequestDto;
        const user = await this.usersService.findOneByEmail(email);
        if (!user) {
            throw new BadRequestException('User does not exist');
        }
        const resetToken = uuidv4();
        const expiration = new Date();
        expiration.setHours(expiration.getHours() + 1); // Token expires in 1 hour
        user.resetToken = resetToken;
        user.resetTokenExpiration = expiration;
        await this.usersService.save(user);

        const mailOptions = {
            to: email,
            subject: 'Password Reset Request',
            template: 'reset-password',
            context: {
                name: user.name,
                token: resetToken,
            },
        };
        await this.mailerService.sendMail(mailOptions);
    }

    async resetPassword(resetPasswordDto: ResetPasswordDto): Promise<void> {
        const user = await this.usersService.findOneBy({
          resetToken: resetPasswordDto.token,
          resetTokenExpiration: MoreThanOrEqual(new Date())
        });
        if (!user) {
          throw new UnauthorizedException('Invalid token');
        }
    
        user.password = await bcryptjs.hash(resetPasswordDto.newPassword, 10);
        user.resetToken = null;
        await this.usersService.save(user);
      }
    
      async verifyResetToken(token: string): Promise<boolean> {
        const user = await this.usersService.findOneBy({
          resetToken: token,
          resetTokenExpiration: MoreThanOrEqual(new Date())
        });
        return !!user;
      }
}
