// src/auth/auth.controller.ts
import { Body, Controller, Query, Get, Render, BadRequestException, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import { ResetPasswordRequestDto } from './dto/reset-request-password.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('register')
    async register(@Body() registerDto: RegisterDto) {
        return this.authService.register(registerDto);
    }

    @Post('login')
    async login(@Body() loginDto: LoginDto) {
        return this.authService.login(loginDto);
    }

    @Post('request-reset-password')
    async requestResetPassword(@Body() resetPasswordRequestDto: ResetPasswordRequestDto) {
        return this.authService.requestPasswordReset(resetPasswordRequestDto);
    }

    @Get('reset-password')
    @Render('reset-password')  // Renderiza la vista de Handlebars para la entrada de la nueva contraseña
    async showResetPasswordForm(@Query('token') token: string) {
        const isValid = await this.authService.verifyResetToken(token);
        if (!isValid) {
            throw new BadRequestException('Invalid or expired token');
        }
        // Renderiza el formulario HTML para ingresar la nueva contraseña
        return { token };  // Pasa el token al contexto del template para usarlo en un campo oculto en el formulario
    }

    @Post('reset-password')
    async changePassword(@Body() resetPasswordDto: ResetPasswordDto) {
        return this.authService.resetPassword(resetPasswordDto);
    }
}
