import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AttendanceService } from './attendance.service';
import { AttendanceController } from './attendance.controller';
import { Attendance } from './entities/attendance.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Attendance])], // Importa TypeOrmModule con la entidad Attendance
  controllers: [AttendanceController],
  providers: [AttendanceService],
})
export class AttendanceModule {}
