import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Attendance {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, user => user.attendances)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Column()
  type: 'entry' | 'exit';

  @Column({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'  // Establece un valor por defecto para MySQL
  })
  timestamp: Date;

  @Column({ nullable: true })
  location?: string;
}
