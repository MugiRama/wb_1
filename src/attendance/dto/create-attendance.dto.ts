import { IsNotEmpty, IsString, IsOptional } from 'class-validator';

export class CreateAttendanceDto {
  @IsString()
  @IsNotEmpty()
  type: 'entry' | 'exit';

  @IsString()
  @IsOptional()
  location?: string;
}
