import { Controller, Post, Body, Req } from '@nestjs/common';
import { AttendanceService } from './attendance.service';
import { CreateAttendanceDto } from './dto/create-attendance.dto';

@Controller('attendance')
export class AttendanceController {
  constructor(private readonly attendanceService: AttendanceService) {}

  @Post()
  async markAttendance(@Body() createAttendanceDto: CreateAttendanceDto, @Req() req: any) {
    return await this.attendanceService.create(createAttendanceDto, req.user);
  }
}
