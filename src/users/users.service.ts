import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository, MoreThanOrEqual } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ){}

  create(createUserDto: CreateUserDto) {
    return this.userRepository.save(createUserDto);
  }

  findOneByEmail(email: string) {
    return this.userRepository.findOneBy({ email });
  }

  // Modificado para incluir verificación de expiración del token
  findOneByResetToken(resetToken: string) {
    return this.userRepository.findOne({
      where: {
        resetToken: resetToken,
        resetTokenExpiration: MoreThanOrEqual(new Date())
      }
    });
  }

  async save(user: User) {
    return this.userRepository.save(user);
  }

  findOneBy(filters: any): Promise<User | null> {
    return this.userRepository.findOneBy(filters);
  }

  findAll() {
    return this.userRepository.find(); // Modificado para realmente retornar todos los usuarios
  }

  findOne(id: number) {
    return this.userRepository.findOneBy({ id });
  }

  async update(userId: number, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.userRepository.findOneOrFail({ where: { id: userId } });
    this.userRepository.merge(user, updateUserDto);
    return this.userRepository.save(user);
  }

  remove(id: number) {
    return this.userRepository.delete(id); // Modificado para realmente eliminar un usuario
  }
}
