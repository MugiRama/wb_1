import { IsEmail, IsNotEmpty, IsOptional, IsString, Length } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsString()
    name: string;

    @Column({ unique: true, nullable: false })
    @IsEmail()
    email: string;

    @Column({ nullable: false })
    @Length(8)
    password: string;

    @Column({ default: 'user' })
    @IsNotEmpty()
    rol: string;

    @Column({ nullable: true })
    @IsOptional()
    resetToken: string;

    @Column({ type: 'timestamp', nullable: true })
    @IsOptional()
    resetTokenExpiration?: Date;

    attendances: any;
}
