import { IsInt, IsBoolean, IsDateString } from 'class-validator';

export class CreateScheduleDto {
  @IsInt()
  userId: number;

  @IsDateString()
  date: string;

  @IsDateString()
  time: string;

  @IsBoolean()
  isEntry: boolean; // Validación para asegurar que se recibe un booleano
}
