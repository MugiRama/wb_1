import { IsInt, IsDateString, IsOptional } from 'class-validator';

export class UpdateScheduleDto {
  @IsInt()
  @IsOptional()
  userId?: number; // Opcional: Asegura que el ID del usuario es un número entero, si se proporciona.

  @IsDateString()
  @IsOptional()
  date?: string; // Opcional: Asegura que la fecha cumple con el formato de fecha ISO, si se proporciona.

  @IsDateString()
  @IsOptional()
  time?: string; // Opcional: Asegura que la hora cumple con el formato de fecha ISO, si se proporciona.
}
