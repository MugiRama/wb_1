import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleService } from './schedule.service';
import { ScheduleController } from './schedule.controller';
import { Schedule } from './entities/schedule.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Schedule])],  // Importa TypeOrmModule con la entidad Schedule
  controllers: [ScheduleController],
  providers: [ScheduleService],
  exports: [ScheduleService]  // Exporta ScheduleService si es necesario en otros módulos
})
export class ScheduleModule {}
